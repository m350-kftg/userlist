import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class UserListTest {

    private User createUser(String eMail) {
        return new User(eMail, 100000L, false);
    }

    @Test
    public void userAddedToListAgainThreeTimesIsIgnored() {
        // given
        UserList userList = new UserList();
        User userAbc1 = createUser("abc@abc.net");
        User userAbc2 = createUser("abc@abc.net");
        User userDef = createUser("def@def.net");
        User userGhi = createUser("ghi@ghi.net");
        // when
        userList.addUser(userAbc1);
        userList.addUser(userAbc2);
        userList.addUser(userDef);
        userList.addUser(userAbc2);
        userList.addUser(userGhi);
        userList.addUser(userAbc2);
        // then
        User[] expectedUsers = {
                createUser("abc@abc.net"),
                createUser("def@def.net"),
                createUser("ghi@ghi.net")
        };
        assertArrayEquals(expectedUsers, userList.getUsers().toArray());
    }
}
