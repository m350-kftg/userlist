import java.util.ArrayList;

public class UserList {

    private ArrayList<User> users;

    public UserList() {
        this.users = new ArrayList<>();
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void addUser(User user) {
        boolean isEqual = false;
        for (User existingUser : users) {
            if (user.equals(existingUser)) {
                isEqual = true;
                break;
            }
        }
        if (!isEqual){
            users.add(user);
        }
    }
}
