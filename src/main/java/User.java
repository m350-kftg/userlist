import java.util.Objects;

public class User {
    private String eMail;
    private long highscore;
    private boolean isLocked;

    public User(String eMail, long highscore, boolean isLocked) {
        this.eMail = eMail;
        this.highscore = highscore;
        this.isLocked = isLocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(eMail, user.eMail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eMail);
    }
}
